import { useState } from "react"
import styled from "styled-components"
import Digimon from "./components/Digimon"
import { useDigimon } from "./providers/Digimons"
import { useMyDigimons } from "./providers/MyDigimons"
import GlobalStyle from "./styles/global"

const Container = styled.div`
	nav {
		background-color: lightgray;
		padding: 2rem;

		ul {
			display: flex;
			gap: 2rem;
			justify-content: center;
			cursor: pointer;
			font-size: 1.5rem;
		}
	}

	div {
		display: flex;
		gap: 1rem;
		padding: 1rem;
		flex-wrap: wrap;
		margin: 0 auto;
	}
`

const App = () => {
	const [isDigimonPage, setIsDigimonPage] = useState<boolean>(true)
	const { digimons } = useDigimon()
	const { myDigimons } = useMyDigimons()

	return (
		<Container>
			<nav>
				<ul>
					<li onClick={() => setIsDigimonPage(true)}>
						Digimons - {digimons.length}
					</li>
					<li onClick={() => setIsDigimonPage(false)}>
						My Digimons - {myDigimons.length}
					</li>
				</ul>
			</nav>
			<div>
				{isDigimonPage ? (
					<>
						{myDigimons.map((digimon) => (
							<Digimon key={digimon.name} digimon={digimon} isFavorite />
						))}
						{digimons
							.filter((d) => !myDigimons.find((dd) => dd.name === d.name))
							.map((digimon) => (
								<Digimon key={digimon.name} digimon={digimon} />
							))}
					</>
				) : (
					myDigimons.map((digimon) => (
						<Digimon key={digimon.name} digimon={digimon} isFavorite />
					))
				)}
			</div>
			<GlobalStyle />
		</Container>
	)
}

export default App
