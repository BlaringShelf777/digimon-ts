import { useState } from "react"
import { useEffect } from "react"
import { useContext } from "react"
import { createContext, ReactNode } from "react"
import { api } from "../../services/api"

interface IDigimon {
	name: string
	img: string
	level: string
}

interface IDigimonProviderProps {
	children: ReactNode
}

interface IDigimonProviderData {
	digimons: IDigimon[]
}

const DigimonContext = createContext<IDigimonProviderData>(
	{} as IDigimonProviderData
)

export const DigimonProvider = ({ children }: IDigimonProviderProps) => {
	const [digimons, setDigimons] = useState<IDigimon[]>([])

	useEffect(() => {
		api
			.get("/")
			.then(({ data }) => setDigimons(data))
			.catch((err) => console.log(err))
	}, [])

	return (
		<DigimonContext.Provider value={{ digimons }}>
			{children}
		</DigimonContext.Provider>
	)
}

export const useDigimon = () => useContext(DigimonContext)
