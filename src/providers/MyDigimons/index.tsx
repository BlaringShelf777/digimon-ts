import { useState } from "react"
import { useContext } from "react"
import { useEffect } from "react"
import { ReactNode } from "react"
import { createContext } from "react"

interface IDigimon {
	name: string
	img: string
	level: string
}

interface IMyDigimonsProps {
	children: ReactNode
}

interface IMyDigimonsProviderData {
	myDigimons: IDigimon[]
	addDigimon: (digimon: IDigimon) => void
	removeDigimon: (digimon: IDigimon) => void
}

const MyDigimonsContext = createContext<IMyDigimonsProviderData>(
	{} as IMyDigimonsProviderData
)

export const MyDigimonsProvider = ({ children }: IMyDigimonsProps) => {
	const [myDigimons, setMyDigimon] = useState<IDigimon[]>([])

	useEffect(() => {
		const storedData = localStorage.getItem("@Digimon:MyDigimons")

		if (storedData !== null) {
			setMyDigimon(JSON.parse(storedData))
		}
	}, [])

	const saveOnStorage = () =>
		localStorage.setItem("@Digimon:MyDigimons", JSON.stringify(myDigimons))

	const addDigimon = (digimon: IDigimon) => {
		if (!myDigimons.find((d) => d.name === digimon.name)) {
			setMyDigimon([...myDigimons, digimon])
			saveOnStorage()
		}
	}

	const removeDigimon = (digimon: IDigimon) => {
		setMyDigimon(
			myDigimons.filter((digimons) => digimons.name !== digimon.name)
		)
		saveOnStorage()
	}

	return (
		<MyDigimonsContext.Provider
			value={{ myDigimons, addDigimon, removeDigimon }}
		>
			{children}
		</MyDigimonsContext.Provider>
	)
}

export const useMyDigimons = () => useContext(MyDigimonsContext)
