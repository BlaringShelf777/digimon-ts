import { ReactNode } from "react"
import { DigimonProvider } from "./Digimons"
import { MyDigimonsProvider } from "./MyDigimons"

interface ProvidersProps {
	children: ReactNode
}

const Providers = ({ children }: ProvidersProps) => {
	return (
		<DigimonProvider>
			<MyDigimonsProvider>{children}</MyDigimonsProvider>
		</DigimonProvider>
	)
}

export default Providers
