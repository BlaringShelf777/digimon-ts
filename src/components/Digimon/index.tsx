import { useMyDigimons } from "../../providers/MyDigimons"
import { Container, Favorite } from "./digimon.style"
import "./styles.css"

interface IDigimon {
	name: string
	img: string
	level: string
}

interface IDigimonProps {
	digimon: IDigimon
	isFavorite?: boolean
}

const Digimon = ({ digimon, isFavorite = false }: IDigimonProps) => {
	const { addDigimon, removeDigimon } = useMyDigimons()
	const { name, img, level } = digimon

	return (
		<Container>
			<img src={img} alt={name} />
			<h3>{name}</h3>
			<p>{level}</p>
			<Favorite
				isFavorite={isFavorite}
				onClick={(evt) => {
					const target = evt.target as HTMLDivElement
					target.classList.toggle("is_animating")
				}}
				onAnimationEnd={() => {
					if (!isFavorite) {
						addDigimon(digimon)
					} else {
						removeDigimon(digimon)
					}
				}}
			/>
		</Container>
	)
}

export default Digimon
