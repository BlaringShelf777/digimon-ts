import styled from "styled-components"

export const Container = styled.div`
	padding: 1rem;
	display: flex;
	flex-direction: column;
	width: fit-content;
	height: 250px;
	border-radius: 0.8rem;
	box-shadow: 0 0 1rem rgba(0, 0, 0, 0.2);
	position: relative;
	justify-content: flex-end;

	img {
		max-height: 150px;
		width: auto;
		margin: 0 auto;
	}

	h3 {
		font-size: 1.5rem;
	}

	p {
		font-size: 1rem;
	}
`

interface IFavorite {
	isFavorite: boolean
}

// https://codepen.io/chrismabry/pen/ZbjZEj
export const Favorite = styled.div`
	cursor: pointer;
	height: 50px;
	width: 50px;
	position: absolute;
	top: 0;
	right: 0;
	background-image: url("https://abs.twimg.com/a/1446542199/img/t1/web_heart_animation.png");
	background-position: ${(p: IFavorite) => (p.isFavorite ? "right" : "left")};
	background-repeat: no-repeat;
	background-size: 2900%;

	&:hover {
		background-position: right;
	}
`
